#!/bin/bash
#
# PingTest CGI 1.0.0
#
# Released on December 12, 2021
#
# pingtest.cgi
#
# Copyright (C) 2021, Anton McClure <anton@tloks.com>.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

title="Ping tests from $HOSTNAME"
count="4"
ttl="30"
hosts=`cat pingtest-hosts`

printf "Content-type: text/html\r\n\r\n"

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">"
echo "<head>"
echo "<meta name=\"generator\" content=\"PingTest CGI 1.0.0\" />"
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
echo "<title>$title</title>"
echo "</head>"
echo "<body>"
echo "<h1 align=\"center\">$title</h1>"

echo "<h2>Hosts pinged</h2>"
echo "<ul>"
for host in $hosts
do
	echo "<li><a href=\"http://$host\">$host</a></li>"
done
echo "</ul>"

echo "<hr />"

echo "<h2>IPv4 Ping time (pinged $count times)</h2>"
echo "<table border=\"1\">"
echo "<tr>"
echo "<th>"
echo "Hostname"
echo "</th>"
echo "<th>"
echo "IP address (IPv4)"
echo "</th>"
echo "<th>"
echo "rtt min"
echo "</th>"
echo "<th>"
echo "rtt avg"
echo "</th>"
echo "<th>"
echo "rtt max"
echo "</th>"
echo "<th>"
echo "rtt mdev"
echo "</th>"
echo "</tr>"
for host in $hosts
do
	if [[ `dig +short a $host` ]]
	then
		result=`ping -4 -qc$count -t$ttl $host | tail -1 | tr '' ' '`
		echo "<tr>"
		echo "<td><a href=\"http://$host\">$host</a></td>"
		host_ipv4=`dig +short a $host`
		echo "<td><a href=\"http://$host_ipv4\">$host_ipv4</a></td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$4" ms":"")}' | cut -c 8-
		echo "</td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$5" ms":"")}'
		echo "</td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$6" ms":"")}'
		echo "</td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$7:"")}'
		echo "</td>"
		echo "</tr>"
		unset result
	fi
	unset host
done
echo "</table>"

echo "<h5><a href=\"#top\">Back to top</a></h5>"

echo "<h2>IPv6 Ping time (pinged $count times)</h2>"
echo "<table border=\"1\">"
echo "<tr>"
echo "<th>"
echo "Hostname"
echo "</th>"
echo "<th>"
echo "IP address (IPv6)"
echo "</th>"
echo "<th>"
echo "rtt min"
echo "</th>"
echo "<th>"
echo "rtt avg"
echo "</th>"
echo "<th>"
echo "rtt max"
echo "</th>"
echo "<th>"
echo "rtt mdev"
echo "</th>"
echo "</tr>"
for host in $hosts
do
	if [[ `dig +short aaaa $host` ]]
	then
		result=`ping -6 -qc$count -t$ttl $host | tail -1`
		echo "<tr>"
		echo "<td><a href=\"http://$host\">$host</a></td>"
		host_ipv6=`dig +short aaaa $host`
		echo "<td><a href=\"http://[$host_ipv6]\">$host_ipv6</a></td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$4" ms":"")}' | cut -c 8-
		echo "</td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$5" ms":"")}'
		echo "</td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$6" ms":"")}'
		echo "</td>"
		echo "<td>"
			echo $result | awk -F'/' 'END{print(/^rtt/?$7:"")}'
		echo "</td>"
		echo "</tr>"
		unset result
	fi
	unset host
done
echo "</table>"

echo "<h5><a href=\"#top\">Back to top</a></h5>"

	echo "<h2>Failed hosts</h2>"
	echo "<table border=\"1\">"
	echo "<tr>"
	echo "<th>No IPv4</th>"
	echo "<th>No IPv6</th>"
	echo "</tr>"
	for host in $hosts
	do
		if [[ -z `dig +short a $host` ]] || [[ -z `dig +short aaaa $host` ]]
		then
			echo "<tr><td valign=\"top\">"
			if [[ -z `dig +short a $host` ]]
			then
				echo "<div><a href=\"http://$host\">$host</a></div>"
			else
				echo "&nbsp;"
			fi
			echo "</td>"
			echo "<td valign=\"top\">"
			if [[ -z `dig +short aaaa $host` ]]
			then
				echo "<div><a href=\"http://$host\">$host</a></div>"
			else
				echo "&nbsp;"
			fi
			echo "</td>"
			echo "</tr>"
		fi
	done
	echo "</table>"
	echo "<h5><a href=\"#top\">Back to top</a></h5>"

echo "<hr />"

echo "<address>Powered by <a href=\"$REQUEST_SCHEME://tloks.com/~anton/pingtest-cgi/\">PingTest CGI</a> v.1.0.0. Copyright &copy; 2021, <a href=\"$REQUEST_SCHEME://tloks.com/~anton/\">Anton McClure</a> &lt;<a href=\"$REQUEST_SCHEME://tloks.com/~anton/contact.html\">anton@tloks.com</a>&gt;. PingTest CGI is licensed under the <a href=\"$REQUEST_SCHEME://tloks.com/~anton/pingtest/LICENSE.txt\">MIT license</a>, which is <a href=\"$REQUEST_SCHEME://opensource.org/licenses/MIT\">Open Source certified</a> and compliant with the <a href=\"$REQUEST_SCHEME://debian.org/social_contract\">Debian Free Software Guidelines</a>. A complete copy of the source code for the version running here is available <a href=\"$REQUEST_SCHEME://tloks.com/~anton/pingtest-cgi/pingtest-cgi-1.0.0.tar.gz\">here</a>.</address>"

echo "</body>"
echo "</html>"
