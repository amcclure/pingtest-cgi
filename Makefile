# PingTest CGI 1.0.0
#
# Released on December 12, 2021
#
# Makefile
#
# Copyright (C) 2021, Anton McClure <anton@tloks.com>.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

TARGET = pingtest
BINDIR = /home/anton/cgi-bin

all: $(TARGET)

install:
	@install -m 755 $(TARGET).bash $(BINDIR)/$(TARGET).cgi
	@install -m 644 LICENSE.txt $(BINDIR)/$(TARGET)-license.txt
	@install -m 644 $(TARGET)-hosts $(BINDIR)/$(TARGET)-hosts

uninstall:
	@rm -f $(BINDIR)/$(TARGET).cgi

.PHONY: all install uninstall
