PingTest CGI: a free CGI-based wrapper for PING(8)
==================================================

Current version:  1.0.0
Released on:      December 12, 2021

PingTest CGI is a free utility that checks network conditions for a
number of hosts in real time. It is written and maintained by Anton
McClure, who started the project in 2021 as a way to easily check the
connectivity of his VPS, even while not on a machine with SSH access.


PREREQUISITES

* APACHE2(8) with the CGI (or CGID) module;
* BIND(2);
* DIG(1);
* GAWK(1);
* TAIL(1);
* TR(1);


INSTALLATION

* Create a host list (1 per line);
* Install with make install;
* Profit;


NOTES

I have a sample version of PingTest CGI running on my personal cgi-bin at
<http://tloks.com/~anton/cgi-bin/pingtest.cgi>.


COPYRIGHT & LICENSE

Copyright (C) 2021, Anton McClure <anton@tloks.com>.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
